package local.scrappers;

import local.ComXLifeApi;
import local.cbr.Packer;
import local.model.ComicData;
import local.model.IssueData;
import local.model.IssueImages;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.RecursiveAction;

import static local.Util.trimAndParseJson;

public class IssueScrapper extends RecursiveAction {
    private static final Logger logger
            = LoggerFactory.getLogger(IssueScrapper.class);

    private final ComicData comicData;
    private final IssueData issue;

    private final String readerURL;

    public IssueScrapper(ComicData comicData, IssueData issue) {
        this.comicData = comicData;
        this.issue = issue;

        readerURL = ComXLifeApi.getReaderUrl(comicData.newsId, issue.id);
    }

    @Override
    protected void compute() {
        logger.info("Начинаю скрапить издание {} №{}", comicData.name, issue.posi);
        Document doc;
        try {
            doc = Jsoup.connect(readerURL).get();

        } catch (IOException e) {
            logger.error("Ошибка загрузки страницы выпуска {}/{}:  {}", comicData.newsId, issue.id, e.getMessage());
            return;
        }

        Element pagesData = doc.selectFirst("body > script:nth-child(6)");

        if(pagesData == null) {
            logger.error("Не удалось получить данные о  страницах издания {} №{}", comicData.name, issue.posi);
            return;
        }

        issue.images = trimAndParseJson(pagesData.data(), IssueImages.class);

        invokeAll(ImageScrapper.collectTasks(comicData, issue));

        Packer.pack(comicData, issue);
    }
}
