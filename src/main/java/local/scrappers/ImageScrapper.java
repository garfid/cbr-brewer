package local.scrappers;

import com.google.common.collect.Lists;
import local.ComXLifeApi;
import local.model.ComicData;
import local.model.ComicImage;
import local.model.IssueData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveAction;
import java.util.stream.Collectors;

public class ImageScrapper{
    private static final Logger logger
            = LoggerFactory.getLogger(ImageScrapper.class);

    public static List<? extends RecursiveAction> collectTasks(ComicData comicData, IssueData issue) {
        logger.info("В издании {} №{} получено {} страниц. Распределяю...", comicData.name, issue.posi, issue.images.references.size());
        List<ComicImage> imageURLS = new ArrayList<>();

        for (int i = 1; i <= issue.images.references.size(); i++) {
            imageURLS.add(new ComicImage(i, ComXLifeApi.getImageUrl(issue.images.references.get(i - 1))));
        }

        return Lists.partition(imageURLS, 4).stream()
                .map(urlsBatch -> new ImageDownloader(comicData, issue, urlsBatch))
                .collect(Collectors.toList());
    }
}
