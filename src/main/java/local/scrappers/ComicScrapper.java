package local.scrappers;

import local.model.ComicData;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.List;
import java.util.concurrent.RecursiveAction;
import java.util.stream.Collectors;

import static local.Util.trimAndParseJson;

public class ComicScrapper extends RecursiveAction {
    private static final Logger logger
            = LoggerFactory.getLogger(ComicScrapper.class);

    private final String rootURL;

    public ComicScrapper(String url) {
        rootURL = url;
    }

    @Override
    protected void compute() {
        logger.info("Начинаю скрапить серию {}", rootURL);
        Document doc;
        try {
            doc = Jsoup.connect(rootURL).get();
        } catch (IOException e) {
            logger.error("Ошибка при загрузке корневой страницы страницы {}: {}", rootURL, e.getMessage());
            return;
        } catch (NullPointerException e) {
            logger.error("Неизвестная ошибка, которая иногда тут случается. Страница {}: {}", rootURL, e.getMessage());
            return;
        }

        ComicData comicData;
        try {
            comicData = getComicData(doc);
        } catch (InvalidObjectException e) {
            logger.error("Ошибка при обработке корневой страницы {}: {}", rootURL, e.getMessage());
            return;
        }

        List<IssueScrapper> worker = comicData.chapters.stream()
                .map(issueData -> new IssueScrapper(comicData, issueData))
                .collect(Collectors.toList());

        invokeAll(worker);
    }

    private ComicData getComicData(Document doc) throws InvalidObjectException, NullPointerException {
        String data;
        Element issueData = doc.selectFirst("div.page__chapters-list > script:first-of-type");

        if(issueData == null) {
            throw new InvalidObjectException("Не удалось найти информации о издании");
        }

        ComicData comicData = trimAndParseJson(issueData.data(), ComicData.class);
        Element headerElem = doc.selectFirst("#dle-content > article > div.page__grid > header > h1");

        if (headerElem == null) {
            throw new InvalidObjectException("Не удалось найти информации о названии серии");
        }

        comicData.name = headerElem.text();
        return comicData;
    }
}
