package local.scrappers;

import local.model.ComicData;
import local.model.ComicImage;
import local.model.IssueData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.concurrent.RecursiveAction;

import static local.Main.*;
import static local.Util.padIndex;

public class ImageDownloader extends RecursiveAction {

    private static final Logger logger
            = LoggerFactory.getLogger(ImageDownloader.class);

    private final ComicData comicData;
    private final IssueData issue;
    private final List<ComicImage> images;

    public ImageDownloader(ComicData comicData, IssueData issue, List<ComicImage> images) {
        this.comicData = comicData;
        this.issue = issue;
        this.images = images;
    }

    @Override
    protected void compute() {
        for (ComicImage image: images) {
            InputStream is;
            try {
                logger.info("В издании {} №{} загружаю страницу {} по адресу {}",
                        comicData.name, issue.posi, image.num, image.url);
                is = new URL(image.url).openStream();
                File file = makeImageFile(image);
                file.createNewFile();
                Files.copy(is, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                logger.error("Ошбика при обработке файлов в издании {} №{} загружаю страницу {} по адресу {}",
                        comicData.name, issue.posi, image.num, image.url);
            }
        }
    }

    private File makeImageDirectory() {
        File dir = new File(new File(SCRAP_ROOT,  comicData.name), String.valueOf(issue.posi));
        dir.mkdirs();
        return dir;
    }

    private File makeImageFile(ComicImage image) {
        String ext = image.url.substring(image.url.lastIndexOf('.') + 1);
        String fileName = String.format("%s.%s", padIndex(image.num, 3), ext);
        File file = new File(makeImageDirectory(), fileName);

        return file;
    }
}
