package local;

import java.util.concurrent.RecursiveAction;

public class ComXLifeApi {
    public static String getImageUrl(String imageReference) {
        return String.format("https://img.com-x.life/comix/%s",imageReference);
    }

    public static String getReaderUrl(int comicId, int issueId) {
        return String.format("https://com-x.life/reader/%d/%d", comicId, issueId);
    }
}
