package local;

import static local.Main.gson;

public class Util {
    public static String padIndex(int index, int size) {
        return String.format("%1$" + size + "s", index).replace(' ', '0');
    }

    public static <T> T trimAndParseJson(String data, Class<T> classType) {
        data = data.substring(data.indexOf('{'), data.lastIndexOf('}')+1);
        return gson.fromJson(data, classType);
    }
}
