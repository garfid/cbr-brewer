package local;

import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.RecursiveAction;
import java.util.stream.Collectors;

public class BootstrapSplit extends RecursiveAction {

    private static final Logger logger
            = LoggerFactory.getLogger(BootstrapSplit.class);
    private final List<String> rootUrls;

    public BootstrapSplit(List<String> args) {
        rootUrls = args;
    }

    @Override
    protected void compute() {
        UrlValidator urlValidator = new UrlValidator();

        List<BootstrapScrap> workers = rootUrls.stream()
                .filter(rootUrl -> {
                    if(!urlValidator.isValid(rootUrl)) {
                        logger.warn("Ссылка {} не является валидным URL и будет пропущена", rootUrl);
                        return false;
                    }

                    return true;
                })
                .map(BootstrapScrap::new)
                .collect(Collectors.toList());
        invokeAll(workers);
    }
}
