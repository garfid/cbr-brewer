package local;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import local.model.ComicData;
import local.model.IssueImages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;

public class Main{
    public static final File COMICS_ROOT = new File(System.getProperty("user.dir") + File.separator + "comics");
    public static final File SCRAP_ROOT = new File(COMICS_ROOT, "raw");
    public static final File CBR_ROOT = new File(COMICS_ROOT, "cbr");
    public static final String FS = File.separator;

    public static final Gson gson = new GsonBuilder()
            .registerTypeAdapter(ComicData.class, new ComicData.ComicDataDeserializer())
            .registerTypeAdapter(IssueImages.class, new IssueImages.IssueDataDeserializer())
            .create();

    private static final Logger logger
            = LoggerFactory.getLogger(Main.class);

    public static String advMkDir(File file) {
        boolean ignored = file.mkdir();

        return file.getAbsolutePath();
    }

    public static void main(String[] args) {
        SCRAP_ROOT.mkdirs();
        CBR_ROOT.mkdirs();

        if(args.length > 0) {
            ForkJoinPool.commonPool().invoke(new BootstrapSplit(Arrays.asList(args)));
        } else {
            logger.warn("список переданных сайтов пуст");
        }
    }
}
