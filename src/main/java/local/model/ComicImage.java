package local.model;

public class ComicImage{
    public final int num;
    public final String url;

    public ComicImage(int num, String url) {
        this.num = num;
        this.url = url;
    }
}
