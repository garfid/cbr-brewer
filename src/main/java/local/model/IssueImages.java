package local.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class IssueImages {
    public List<String> references;

    public IssueImages(List<String> references) {
        this.references = references;
    }

    public static class IssueDataDeserializer implements JsonDeserializer<IssueImages> {

        @Override
        public IssueImages deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject obj = json.getAsJsonObject();

            List<String> images = new ArrayList<>();

            for (JsonElement imagesJson : obj.get("images").getAsJsonArray()) {
                images.add(imagesJson.getAsString());
            }

            return new IssueImages(images);
        }
    }
}
