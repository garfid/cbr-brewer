package local.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class IssueData {
    public int id;
    public int posi;

    public IssueImages images;

    public IssueData(int id, int posi) {
        this.id = id;
        this.posi = posi;
    }
}
