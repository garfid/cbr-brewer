package local.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ComicData {
    public int newsId;
    public String name;
    public List<IssueData> chapters;

    public ComicData(int newsId, List<IssueData> chapters) {
        this.newsId = newsId;
        this.chapters = chapters;
    }

    public static class ComicDataDeserializer implements JsonDeserializer<ComicData> {
        @Override
        public ComicData deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject root = json.getAsJsonObject();
            return new ComicData(
                    root.get("news_id").getAsInt(),
                    brewIssues(root.get("chapters"), typeOfT, context)
            );
        }

        public List<IssueData> brewIssues(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
            List<IssueData> issues = new ArrayList<>();

            JsonArray issuesJson = json.getAsJsonArray();

            for (JsonElement issue: issuesJson) {
                JsonObject issueObj = issue.getAsJsonObject();
                issues.add(new IssueData(issueObj.get("id").getAsInt(), issueObj.get("posi").getAsInt()));
            }

            return issues;
        }
    }
}
