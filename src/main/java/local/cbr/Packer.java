package local.cbr;

import com.google.common.io.Files;
import local.ComXLifeApi;
import local.model.ComicData;
import local.model.IssueData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static local.Main.*;
import static local.Util.padIndex;
import static local.cbr.ZipDirectory.zip;

public class Packer{
    private static final Logger logger = LoggerFactory.getLogger(Packer.class);

    public static void pack(ComicData comicData, IssueData issueData) {
        File sourceDir = new File(new File(SCRAP_ROOT, comicData.name), String.valueOf(issueData.posi));

        String issueNumber =  padIndex(issueData.posi, 2);
        String cbrName = String.format("%s #%s", comicData.name, issueNumber);
        String packDate = new SimpleDateFormat("yyyy.MM").format(new Date());
        String l1Name = String.format("%s [%s] #%s", comicData.name, packDate, issueNumber);

        File l1File = new File(new File(CBR_ROOT, cbrName), l1Name);

        l1File.mkdirs();

        File[] files = sourceDir.listFiles();

        if(files == null) {
            logger.error("В издании {} №{} не оказалось загруженных изображений", comicData.name, issueData.posi);
            return;
        }

        for(int p = 0; p < files.length; p++) {
            File image = files[p];
            String fileName = getFileName(comicData.name, issueNumber, p, image);
            File page = new File(l1File, fileName);

            try {
                boolean ignored = page.createNewFile();
            } catch (IOException e) {
                logger.error("Ошибка при создании файл {}:", page.getAbsolutePath(), e);
            }

            try {
                Files.copy(image, page);
            } catch (IOException e) {
                logger.error("Ошибка при копировании файла {}=>{}:", image.getAbsolutePath(), page.getAbsolutePath(), e);
            }

        }

        try {
            zip(new File(CBR_ROOT, cbrName), cbrName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getFileName(String comicName, String issueNumber, int pageIndex, File image) {
        String suffix = pageIndex == 0 ? "c01" : "p" + padIndex(pageIndex, 3);

        String ext = Files.getFileExtension(image.getPath());
        String fileName = String.format("%s_%s_%s.%s", comicName, issueNumber, suffix, ext);
        return fileName.replace(' ', '_');
    }
}
