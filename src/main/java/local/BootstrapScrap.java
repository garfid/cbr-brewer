package local;

import local.scrappers.ComicScrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.RecursiveAction;

public class BootstrapScrap extends RecursiveAction {
    private static final Logger logger
            = LoggerFactory.getLogger(BootstrapScrap.class);

    private final String rootUrl;

    public BootstrapScrap(String rootUrl) {
        this.rootUrl = rootUrl;
    }

    @Override
    protected void compute() {
        logger.info("Начинаю выгружать {}", rootUrl);
        try {
            invokeAll(new ComicScrapper(rootUrl));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            logger.error("Ошибка инциализации скрапера для адреса {}: {}", rootUrl, e.getMessage());
        }
    }
}
